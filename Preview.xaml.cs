﻿using EASendMailRT;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.ApplicationModel.Email;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace CameraStarterKit
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Preview : Page
    {

        private StorageFile csvFile = null;
        private async void CheckFile()
        {
            try
            {
                csvFile = await KnownFolders.PicturesLibrary.GetFileAsync(DateTime.Now.ToString("yyyyMMdd") + ".csv");
            }
            catch (FileNotFoundException e)
            {
                await KnownFolders.PicturesLibrary.CreateFileAsync(DateTime.Now.ToString("yyyyMMdd") + ".csv");
            }

        }

        public Preview()
        {
            CheckFile();
            this.InitializeComponent();
            waitDisplay();
            StartTimer();
        }

        private int retryCount = 0;
        private async void waitDisplay()
        {
            if (!Helper.PhotoRendered)
            {
                retryCount++;
                Debug.WriteLine("Retry: " + retryCount);
                await System.Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(100));
                waitDisplay();
            }
            else
            {
                SendEmailAsync();

                StorageFile file = await KnownFolders.PicturesLibrary.GetFileAsync(@"output\output_" + Helper.LastOutputFile + ".jpeg");
                using (var imageStream = await file.OpenReadAsync())
                {
                    var bitmapImage = new BitmapImage();
                    await bitmapImage.SetSourceAsync(imageStream);
                    preview.Source = bitmapImage;

                }
                Helper.PhotoRendered = false;
                retryCount = 0;
            }
        }

        public async System.Threading.Tasks.Task SendEmailAsync()
        {
            try
            {
                SmtpMail oMail = new SmtpMail("TryIt");
                EASendMailRT.SmtpClient oSmtp = new EASendMailRT.SmtpClient();

                // Set sender email address, please change it to yours
                oMail.From = new MailAddress("CQCPinkfong@thephotobooth.company");

                // Add recipient email address, please change it to yours
                int validEmails = 0;


                for (int i = 0; i < Helper.toEmails.Length; i++)
                {
                    StringBuilder sb = new StringBuilder();
                    string newLine = Helper.toEmails[i] + "," + DateTime.Now.ToString("yyyyMMddHHmmss") + "," + Helper.LastOutput + "," + Environment.NewLine;
                    sb.Append(newLine);

                    FileIO.AppendTextAsync(csvFile, sb.ToString());

                    if (IsValidEmail(Helper.toEmails[i]) == true)
                    {
                        oMail.To.Add(new MailAddress(Helper.toEmails[i]));
                        validEmails++;
                    }
                }

                if(validEmails > 0)
                {
                    // Set email subject and text body
                    oMail.Subject = "Adventure with Pinkfong at Clarke Quay Central";
                    oMail.HtmlBody = @"<p><span style='font-family: StarlingTextRoman, serif'>Hi There,</span><br></p><p><span style='font-family: StarlingTextRoman, serif'>Here is your captured moment with Pinkfong and Baby Shark!</span><br></p><p><span style='font-family: StarlingTextRoman, serif'>Remember to join the #CQCPinkfong contest by sharing your picture with Pinkfong or Baby Shark at Clarke Quay Central! 3 winners will receive a plush toy and a sticker book!</span><br></p><p><span style='font-family: StarlingTextRoman, serif'>Like Clarke Quay Central Facebook, follow us on Instagram and include #CQCPinkfong in your post to qualify.</span><br></p><p><span style='font-family: StarlingTextRoman, serif'>Thank you for visiting us at Clarke Quay Central!</span><br></p><p><u>(Please&nbsp;do&nbsp;not respond to this computer-generated message.)</u><br></p><div><br></div><div><br></div>";

                    Windows.Storage.StorageFile file =
                        await Windows.Storage.KnownFolders.PicturesLibrary.GetFileAsync(@"output\output_" + Helper.LastOutputFile + ".jpeg");
                    string attFile = file.Path;
                    Attachment oAttachment = await oMail.AddAttachmentAsync(attFile);

                    SmtpServer oServer = new SmtpServer("smtp.zoho.com");

                    oServer.User = "CQCPinkfong@thephotobooth.company";
                    oServer.Password = "CQC2018PinkFong";

                    oServer.Port = 465;
                    oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;

                    oSmtp.SendMailAsync(oServer, oMail);
                }

                
            }
            catch (Exception ex)
            {
            }

        }

        private static bool IsValidEmail(string email)
        {
            return Regex.IsMatch(email, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }

        private int countdown = 0;
        private Boolean resetted = false;

        private async void StartTimer()
        {
            if (!resetted)
            {
                if (countdown >= 10000)
                {
                    Frame.Navigate(typeof(Begin));
                }
                else
                {
                    await System.Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(1000));
                    countdown = countdown + 1000;
                    Debug.WriteLine("Time out: " + countdown);
                    StartTimer();
                }
            }
            
        }

        private void Preview_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Begin));
            resetted = true;
        }
    }
}
