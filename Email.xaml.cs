﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace CameraStarterKit
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Email : Page
    {
        public Email()
        {
            this.InitializeComponent();
        }

        private void keyboardButtonClicked(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn.Content.ToString() == "backspace")
            {
                if (toAddressBox.Text.Length > 0)
                {
                    toAddressBox.Text = toAddressBox.Text.Substring(0, toAddressBox.Text.Length - 1);
                }
            }
            else
            {
                toAddressBox.Text += btn.Content.ToString();
            }

        }

        private void NextButtonClicked(object sender, RoutedEventArgs e)
        {
            NextButton.IsEnabled = false;
            string emails = Regex.Replace(toAddressBox.Text, @"\s", "");
            string[] toEmails = emails.Split(';');
            Helper.toEmails = toEmails;

            Frame.Navigate(typeof(MainPage));
        }

        private void BackButtonClicked(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(PDPA));
        }
    }
}
